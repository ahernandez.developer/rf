import cv2
import os
import numpy as np

dataPath = "C:/Users/PC/Documents/python/RF/FacesBank"

personalList = os.listdir(dataPath)

labels = []
facesData = []
label = 0

for nameDir in personalList:
    personPath = dataPath + "/" + nameDir
    for fileName in os.listdir(personPath):
        labels.append(label)
        facesData.append(cv2.imread(personPath+"/"+fileName,0))
        image = cv2.imread(personPath+"/"+fileName,0)
        cv2.imshow('image',image)
        cv2.waitKey(10)
    label = label+1

face_recognizer = cv2.face.EigenFaceRecognizer_create()

#entrenamiento
print("Entrenando...")
face_recognizer.train(facesData,np.array(labels))

#
face_recognizer.write('modelEigenFace.xml')

print("StoredModel")
