import cv2
import os

bankPath = "C:/Users/PC/Documents/python/RF/src"

imagesPathList = os.listdir(bankPath)
    

if not os.path.exists("FacesBank"):
    print("created dir")
    os.makedirs('FacesBank')

faceClassif = cv2.CascadeClassifier(cv2.data.haarcascades+"haarcascade_frontalface_default.xml")

count = 0
for imageName in imagesPathList:
    image = cv2.imread(bankPath+"/"+imageName)
    imageAux = image.copy()
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    faces = faceClassif.detectMultiScale(gray,1.1,5)

    for(x,y,w,h) in faces:
        cv2.rectangle(image,(x,y),(x+w,y+h),(128,0,255),2)
        cv2.imshow('image',image)    
    k = cv2.waitKey(0)
    if k == ord('s'):
        for(x,y,w,h) in faces:
            face = imageAux[y:y+h,x:x+w]
            face = cv2.resize(face,(150,150),interpolation=cv2.INTER_CUBIC)
            cv2.imshow('face',face)
            cv2.waitKey(0) 
            cv2.imwrite("FacesBank/face_{}.jpg".format(count),face)
            count = count+1
    elif k == 27:
        break            
cv2.destroyAllWindows() 