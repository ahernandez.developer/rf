import cv2
import os
dataPath = "C:/Users/PC/Documents/python/RF/FacesBank"
imagePaths = os.listdir(dataPath)

face_recognizer = cv2.face.EigenFaceRecognizer_create()

face_recognizer.read('modelEigenFace.xml')

cap = cv2.VideoCapture(0)

faceClassif = cv2.CascadeClassifier(cv2.data.haarcascades+"haarcascade_profileface.xml")

while True:
    ret,frame = cap.read()
    if ret == False: break
    gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    auxFrame = gray.copy()

    faces = faceClassif.detectMultiScale(gray,1.3,5)

    for (x,y,w,h) in faces:
        face = auxFrame[y:y+h,x:x+w]
        face = cv2.resize(face,(350,350),interpolation=cv2.INTER_CUBIC)
        result = face_recognizer.predict(face)
        if(result[1]<70000):
            cv2.putText(frame,'{},{}'.format(imagePaths[result[0]],result[1]),(x,y-5),1,1.3,(255,255,0),1,cv2.LINE_AA)
        else:
            cv2.putText(frame,'{},{}'.format("Desconocido",result[1]),(x,y-5),1,1.3,(255,255,0),1,cv2.LINE_AA)
        cv2.rectangle(frame,(x,y),(x+w,y+h),(128,0,255),2)

    cv2.imshow('frame',frame)
    k = cv2.waitKey(1)
    if k == 27: break

cap.release()
cv2.destroyAllWindows()
