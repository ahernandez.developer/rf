import cv2
import os

if not os.path.exists("FacesBank"):
    os.makedirs("FacesBanks")

cap = cv2.VideoCapture(0,cv2.CAP_DSHOW)

faceClassif = cv2.CascadeClassifier(cv2.data.haarcascades+"haarcascade_frontalface_default.xml")

count = 0
while True:
    ret,frame = cap.read()
    frame = cv2.flip(frame,1)
    gray =  cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    auxFrame = frame.copy()

    faces = faceClassif.detectMultiScale(gray,1.3,5)

    k = cv2.waitKey(1)
    if k == 27:
        break
    
    for(x,y,w,h) in faces:
        cv2.rectangle(frame,(x,y),(x+w,y+w),(0,255,0),2)
        face = auxFrame[y:y+h,x:x+w]
        face = cv2.resize(face,(150,150),interpolation=cv2.INTER_CUBIC)

        if k == ord('s'):
            cv2.imwrite("FacesBank/face_{}.jpg".format(count),face)
            cv2.imshow('face',face)
            count = count+1
        cv2.rectangle(frame,(10,5),(450,25),(255,255,255,),-1)
        cv2.putText(frame,"Press s to save the found faces",(10,20),2,0.5,(128,0,255))
        cv2.imshow('frame',frame)
cap.release()
cv2.destroyAllWindows()        